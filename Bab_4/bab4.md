# Bab 4 Implementasi sains terbuka

Pembahasan kami bagi menjadi dua bagian: 

1. ranah riset, 
2. ranah publikasi, dan 
3. ranah komunikasi sains

## 4.1 Ranah riset

Dalam implementasi riset, sangat disarankan untuk membuat repositori proyek. Repositori ini menyimpan berbagai komponen dalam riset:

- **data**, antara lain berisi: data mentah (*raw data*), data terproses (*processed data*)
- **metode**, antara lain berisi: hasil eksperimen, hasil pengujian laboratorium, kode program, foto lapangan
- **luaran**, antara lain berisi: grafik-grafik dan tabel hasil analisis
- **laporan**, antara lain berisi: manuskrip laporan riset
- **publikasi**, antara lain berisi: manuskrip makalah, tayangan presentasi, press release, naskah blog

### 4.1.1 Data terbuka (*open data*)

Data terbuka muncul [bukan hanya karena masalah akses, tetapi juga kultur](http://dasaptaerwin.net/wp/2018/01/open-data-workshop-itb-unversity-of-sydney-material-pre-workshop.html). Di bawah ini adalah hasil survey daring kecil (38 responden) untuk mengetahui pengetahuan peneliti tentang data terbuka. Ternyata 26 orang diantaranya setuju dengan data terbuka atau aktivitas membuka data. Namun demikian hanya 57% diantaranya yang telah mulai membagikan data secara terbuka. 

![survey-opendata](survey-opendata.png)

![survey-opendata2](survey-opendata2.png)

Masalah akses sangat penting karena: pengambilan data memerlukan sumber daya (dana, waktu, upaya, sumber daya manusia) yang besar. Seringkali data yang telah dianalisis, masih menyisakan pertanyaan yang dapat dijawab oleh peneliti lainnya. Oleh karena itu, data perlu dibuka seluas-luasnya. [Sejauh mana data dapat dibuka](https://podcasts.ox.ac.uk/how-open-should-open-data-be)? Tentunya ada batasan yang akan dibahas pada sub bab yang lain.  Beberapa alasan yang berkaitan dengan akses dijelaskan dengan baik oleh [Open Knowledge Network International](https://okfn.org/opendata/why-open-data/):

1. **untuk meningkatkan transparansi**: luaran dari kegiatan riset perlu dijabarkan secara transparan, tidak terkecuali data.
2. **untuk meningkatkan inovasi, nilai sosial dan komersial**: data dapat digunakan ulang oleh berbagai pihak untuk menjawab masalahnya masing-masing dan pada gilirannya menghasilkan inovasi. Inovasi diharapkan dapat menyelesaikan masalah sosial, selain juga nilai komersial.
3. **untuk menggalang partisipasi**: data yang dibuka dapat memicu komunikasi yang kemudian diharapkan dapat menggalang partisipasi dari berbagai pihak. Seorang peneliti yang memiliki data tapi belum berhasil menelaahnya secara lengkap, dapat minta bantuan pihak lain untuk menganalisisnya, bahkan melengkapinya.

![padlock](https://cdn.pixabay.com/photo/2015/03/26/09/46/padlock-690286_960_720.jpg)

Data terbuka juga masalah kultur (budaya). Mari kita bercermin ke pengalaman saat akan melaksanakan penelitian. Sering kita bingung untuk menentukan kualitas suatu literatur di saat datanya sendiri tidak diunggah secara lengkap dan transparan. Kita sebagai peneliti juga sering frustrasi, apakah benar suatu daerah atau masalah belum pernah ada yang mengkaji. Ini sering menghambat riset kita. Pada waktu yang berbeda, kita juga tidak merasa perlu untuk membuka data kita untuk orang lain. Jelas ini masalah kultur yang sangat ingin dibantu, tapi tidak ingin membantu. Ataukah karena tidak tahu caranya? Kami sangat berharap ini yang terjadi.

Kami merekomendasikan pembaca untuk membaca makalah ini [Promoting data sharing among Indonesian scientists: A proposal of generic university-level Research Data Management Plan (RDMP)](https://riojournal.com/articles.php?id=28163) yang menjelaskan cara praktis membagikan data.

### 4.1.2 Metode terbuka (open method)

...

...

##4.2 Ranah publikasi

4.2.1 Memilih jurnal

4.2.2 Peninjauan terbuka (*open review*)

4.2.3 .....



## 4.3 Ranah komunikasi sains

### 4.3.1 xxx

...

### 4.3.2 xxx

...

### 4.3.3 xxx

...





