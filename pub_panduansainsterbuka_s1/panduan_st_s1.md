---
title: "Panduan sainsterbuka untuk mahasiswa S1"
output:
  html_document:
    theme: united
    toc: true
    toc_depth: 3
    highlight: tango
    number_sections: true
author: Dasapta Erwin Irawan (Tim Sains Terbuka)
---

# Panduan sainsterbuka untuk mahasiswa S1

Oleh Dasapta Erwin Irawan (Tim Sains Terbuka) 

**Tujuan**: Panduan singkat ini ditulis untuk menyebarkan sainterbuka kepada para mahasiswa. 

**Target pembaca**: mahasiswa S1

**Latar belakang**: mahasiswa S1 adalah calon-calon penggerak bangsa, maka mereka perlu menerima wawasan-wawasan baru yang membuka pikiran sebagai bekal untuk membangun bangsa.

**Referensi utama**: [Strategi Sains Terbuka](https://sainsterbuka.readthedocs.io/en/latest/strategi.html#strategi-jangka-pendek-2-tahun), 

**Tim Sainsterbuka:** [Surya Dalimunthe](https://sites.google.com/site/tamanbahasa), [Afrilya](https://sainsterbuka.readthedocs.io/en/latest/www.afrilya.com), [Juneman Abraham](http://psychology.binus.ac.id/D3728), [Dasapta Erwin Irawan](http://dasaptaerwin.net/wp/), [Sami Kandha Dipura](https://scholar.google.co.id/citations?user=-ug02sIAAAAJ&hl=id)

# Pendahuluan

Tujuan pendidikan S1/sarjana dari sudut pandang ilmu kebumian. Walaupun didekati dari ilmu kebumian, tetapi sepertinya mirip dengan bidang ilmu lainnya. Mohon masukannya tentang hal ini:

1. mampu mengambil data dengan baik,
2. mampu menyajikan data dengan baik,
3. mampu menganalisisnya untuk menjawab masalah dengan ilmu yang sudah dimiliki.

Ketiganya sudah dibahas dalam matakuliah-matakuliah Metode Penelitian, atau yang sejenisnya. Tapi masih sangat sedikit bahan ajar yang menyampaikan teknik berbagi informasi (data, analisis, dan hasil). Kembali ke tujuan di atas, tujuan ke-1 dan ke-2 adalah yang utama menurut kami. 

# Sekilas tentang sainsterbuka

> Sains Terbuka adalah praktik yang berdasarkan hak dan kewajiban untuk menggunakan, menyesuaikan, dan menyebarluaskan ilmu pengetahuan secara terbuka, sehingga setiap orang dapat berkolaborasi dan berkontribusi lewat ide, data, metode, tulisan dan semua unsur-unsur lain yang terkait ilmu pengetahuan [tautan].

Sainsterbuka bukanlah cara baru dalam penelaahan ilmiah, melainkan cara yang seharusnya. Prinsip dasarnya adalah bahwa seorang peneliti perlu menyebarkan hasil risetnya atau kegiatan ilmiahnya secara terbuka. Terbuka di sini tidak sekedar membagikan berkas hasil penelitian dalam format PDF secara daring, melainkan juga melakukan beberapa hal berikut ini (diurutkan dari yang paling mudah ke yang paling sulit).

Tabel 1 Beberapa kegiatan berbagi hasil riset (*research sharing*) yang sering dilakukan dan tingkat kesulitannya (berkas orisinal `tingkatkesulitan.csv`)

| tingkat kesulitan | kegiatan                                                     | tahap     | pelatihan | keterangan                                                   |
| ----------------- | ------------------------------------------------------------ | --------- | --------- | ------------------------------------------------------------ |
| rendah            | membagikan data dan metode dalam berkas dalam berkas terpisah | analisis  |           |                                                              |
| tinggi            | membagikan data dan metode ke repositori atau platform khusus | analisis  | perlu     | Contoh repositori: OSF, Figshare, Zenodo. Contoh repositori data khusus: Pangaea, Contoh repositori metode: Protocols |
| tinggi            | bekerja secara sistematis menggunakan bahasa pemrograman (coding) untuk menghasilkan riset yang dapat diulang (reproducible research). | analisis  | perlu     | Kebanyakan orang Indonesia lebih senang dengan aplikasi modus klik, bukan pemrograman. Secara praktikal, mungkin modus klik lebih mudah dipelajari, tapi tidak mudah untuk dapat membagikan urutan kerjanya kepada orang lain. Sebagian orang melakukan tangkap layar (screenshot) untuk menampilkan urutan kerja, tapi ini jelas tidak praktis, salah satunya karena menghasilkan kumpulan gambar yang berukuran besar. |
| tinggi            | menggunakan piranti keras (PK) yang terbuka (bebas untuk ditiru dan digandakan) dalam analisis | analisis  | perlu     | Menurut kami ini paling susah dilakukan, karena kita sudah banyak bergantung kepada PK komersial buatan luar negeri. Namun demikian, banyak juga peneliti yang membuka alat buatannya agar mudah ditiru dan digandakan oleh peneliti lainnya. |
| rendah            | membagikan laporan, artikel, poster, abstrak secara daring dalam format PDF | pelaporan | tidak     |                                                              |
| rendah            | membagikan laporan, artikel, poster, abstrak secara daring dalam format non-PDF: odt (format LibreOffice) atau docx (MS Office) | pelaporan | tidak     | Khusus untuk format docx dkk (pptx, xlsx), sering dipilih karena memang PL Microsoft Office memang sudah seperti standar baku dalam operasi riset di Indonesia |
| rendah            | membagikan slide presentasi sidang atau seminar secara daring, ke repositori atau ke media lain | pelaporan | tidak     | Untuk mengunggah slide ke Slideshare tidaklah sulit. Tapi perlu dipahami kalau Slideshare adalah platform komersial. Mengunggah slide ke repositori tetap direkomendasikan. |
| tinggi            | membagikan laporan, artikel, poster, abstrak secara daring dalam format non-PDF agar lebih mudah digunakan orang lain, misal format: teks ASCII, markdown, html, latex. | pelaporan | perlu     |                                                              |
| rendah            | membagikan proposal                                          | proposal  | perlu     | Yang perlu dilatih di sini antara lain: memilah substansi apa yang dapat dibagikan dan mana yang tidak, masalah perjanjian penggunaan data, dan bagaimana caranya agar proposal (dan dokumen lainnya) dapat dicari secara daring (searchable) oleh peneliti lainnya. |

# Panduan untuk mahasiswa S1

## Kondisi dasar

1. Kami membuat panduan ini sebagai **himbauan**, adapun kami juga memahami bila mahasiswa tidak dapat melaksanakannya, karena peraturan perguruan tinggi tidak membolehkannya. Jadi mohon dapat memeriksa peraturan internal masing-masing sebelum melaksanakan rekomendasi kami.
2. **Riset yang disebarkan merupakan riset dengan biaya sendiri**. Kenapa? Sumber biaya adalah salah satu kunci saat memutuskan apakah hasil riset dapat dipublikasikan secara luas ataukah hanya secara terbatas di perpustakaan perguruan tinggi. Mahasiswa harus berkonsultasi dengan pembimbing atau pemberi dana, tentang mana yang boleh dibagikan dan mana yang tidak boleh dibagikan.
3. **Riset tidak mengandung hal yang sensitif atau rahasia**. Beberapa hal yang dikategorikan sensitif dan rahasia dapat dilihat di [Repositori Pelatihan Data Terbuka ini](https://osf.io/s76gu/).
4. **Riset tidak mendiskreditkan suatu pihak**. Bila ada kritik, maka perlu disampaikan secara santun dengan dukungan data.  
5. **Sainsterbuka dimulai tidak untuk membuat kesulitan baru**. Oleh karenanya silahkan sesuaikan langkah yang Anda pilih dengan sumber daya yang Anda punya: dana, piranti lunak, piranti keras, waktu, dll yang terkait.
6. **Dokumen tidak mengandung plagiarisme**. Ini memerlukan kehati-hatian penulis, karena sekali dokumen daring, maka jejaknya akan dapat dilacak, sekalipun dokumennya telah dihapus.
7. **Penulis memegang Hak Cipta untuk seluruh dokumen yang dihasilkannya**. Oleh karenanya, ia bertanggungjawab penuh atas dokumen tersebut. 

## Perangkat yang diperlukan

1. Akun ORCID. Anda bisa membuatnya secara gratis menggunakan akun surel yang dimiliki. Daftar di [ORCID](https://orcid.org). Mengapa ORCID? Selain karena gratis, akun ORCID dapat menangkap karya-karya Anda yang telah diunggah secara daring. Jadi Anda bisa punya profil daring.
2. Akun [Google Scholar (GS)](https://scholar.google.com). Untuk mendampingi akun ORCID, Anda juga dapat membuat akun GS. Mirip dengan ORCID, akun GS akan menyimpan data karya daring Anda.
3. Cek di perguruan tinggi Anda, apakah mereka menyediakan repositori institusi? Berikut beberapa repositori Indonesia yang masuk ke dalam [daftar Open DOAR](http://www.opendoar.org/find.php?cID=100&title=Indonesia) (open directory of open access repository).
4. Kalau perguruan tinggi Anda belum menyediakan repositori institusi, Anda bisa membuat akun di repositori terbuka yang gratis, seperti: [Open Science Framework (OSF)](osf.io), [Figshare](https://figshare.com), dan [Zenodo](zenodo.org).  
5. Bila Anda menginginkannya, Anda dapat pula membuat akun di media penyimpanan slide, seperti [Slideshare](slideshare.net) atau [Speakerdeck](speakerdeck.com). 
6. Bagi Anda yang kuliah di prodi terkait dengan komputer atau pemrograman, kami anjurkan untuk membuat akun di repositori pemrograman seperti [Gitlab](gitlab.com) atau [Github](github.com). Dalam perkembangannya, ternyata saat ini Gitlab dan Github bisa dimanfaatkan untuk menyimpan karya-karya yang tidak berkaitan dengan pemrograman.
7. Gunakan piranti lunak kode bebas dan gratis. Aplikasi seperti ini telah banyak tersedia untuk segala kebutuhan, misal: Anda dapat memilih [LibreOffice](libreoffice.org) bukan Microsoft Office untuk mengolah teks, [Statcal](https://statcal.info/) dan bukan SPSS.   

## Kegiatan yang direkomendasikan

### Kegiatan 1: Bagikan tugas kuliah Anda

Anda pasti sudah bersusah payah menyelesaikan setiap tugas yang diberikan dosen. Siapa yang akan membacanya selain Anda dan tentunya dosen Anda? Tidak ada. Akan lebih baik kalau Anda bagikan dengan mengunggahnya ke repositori atau di media yang Anda sukai. Orang atau mahasiswa lain mungkin akan belajar banyak dari dokumen Anda.

![task list](https://c.pxhere.com/photos/f2/63/to_do_list_task_list_notes_written-883706.jpg!d) 

Ilustrasi task list ([Pxhere, CC-0](https://c.pxhere.com/photos/f2/63/to_do_list_task_list_notes_written-883706.jpg!d))

### Kegiatan 2: Bagikan catatan kuliah Anda

Dasar pemikirannya sangat sederhana. Anda akan mencatat untuk kepentingan Anda sendiri. Anda bebas menggunakan berbagai teknik mencatat, misal pemetaan pikiran (*mindmapping*), [Cornell *note taking*](https://medium.goodnotes.com/study-with-ease-the-best-way-to-take-notes-2749a3e8297b), catatan visual ([*visual note taking*](https://www.verbaltovisual.com/an-introduction-to-visual-note-taking/), contoh: [flickr/dasaptaerwin](flickr.com/photos/d_erwin_irawan)). Mencatat akan lebih baik, kalau Anda berpikir bahwa orang lain akan membacanya. Oleh karenanya, bagikan catatan kuliah Anda melalui medsos misalnya. Selain Anda diharapkan dapat mencatat dengan lebih baik, karena akan dibaca orang lain, mendatangkan manfaat bagi yang membaca, juga mungkin dapat menjadi awalan untuk berkomunikasi dengan pembaca, yang mungkin adalah mahasiswa juga.

![contoh peta pikiran](https://www.volksuniversiteithetgooi.nl/media/com_hikashop/upload/inf-447_2_x_zo_snel_lezen.jpg)

Ilustrasi peta pikiran ([volksuniversiteithetgooi, CC-BY](https://www.volksuniversiteithetgooi.nl/media/com_hikashop/upload/inf-447_2_x_zo_snel_lezen.jpg))

### Kegiatan 3: Buat dokumentasi yang sistematis saat Anda beraktivitas

Coba buat dokumentasi yang sistematis untuk setiap kegiatan. Zaman sekarang banyak yang membuat vlog (*video log*). Tapi jarang kami lihat vlog yang berkaitan dengan tugas akhir atau kegiatan akademik lainnya. Dokumentasi juga dapat berupa foto. Anda dapat mengunggahnya ke media sosial (misal [Instagram](instagram.com) atau [Flickr](flickr.com)) dengan membuat sedikit cerita. Jangan lupa membuat tagar yang sesuai. Ada satu hal yang hampir tidak pernah dilakukan oleh pengguna media sosial, yakni menyisipkan lisensi untuk foto dan video yang Anda unggah. Lisensi adalah hak-hak yang Anda berikan untuk para pembaca atau pengunjung. 

Berikan hak yang cukup luas untuk pengunjung, agar mereka dapat menggunakan ulang karya untuk untuk kegiatan mereka. Kami merekomendasikan lisensi [CC-BY](https://creativecommons.org/licenses/by/2.0/). Dengan menempelkan lisensi ini, maka Anda memberikan kebebasan kepada pengunjung untuk menggunakan ulang karya Anda secara bebas dengan syarat menyebutkan sumbernya atau pembuat orisinilnya.  Untuk pengguna [Youtube](youtube.com) dan [Flickr](flickr.com), Anda diberikan form untuk memilih lisensi. Tapi untuk Instagram, Anda bisa menyisipan `CC-BY` di akhir teks statusnya. 

Contoh:

`Observasi batuan beku di kawasan Gunung Salak, Bogor. Terlihat adanya aliran lava basalt membentang dari timur ke barat (Lisensi gambar CC-BY)`

![Ilustrasi dokumentasi lapangan](https://c.pxhere.com/photos/64/0d/teamwork_farm_workers_co_operation_farm_hay_worker_agriculture_field-753718.jpg!d)

Ilustrasi dokumentasi lapangan ([Pxhere, CC-0](https://c.pxhere.com/photos/64/0d/teamwork_farm_workers_co_operation_farm_hay_worker_agriculture_field-753718.jpg!d)) 

### Kegiatan 4: Bagikan data juga

Untuk setiap tugas atau dokumen yang Anda bagikan, bagikan juga datanya. Banyak yang tidak setuju, tapi kalau Anda dapat mengunggah data di repositori, maka pembaca yang memerlukan dapat menggunakannya dan menyitirnya (merujuknya). Data dibagikan dalam berkas (*file*) terpisah, sebagai tabel dengan format teks, misal csv atau txt.

![Ilustrasi berbagi data, Wikimedia Commons, CC-0](https://upload.wikimedia.org/wikipedia/commons/6/65/To_deposit_or_not_to_deposit%2C_that_is_the_question_-_journal.pbio.1001779.g001.png)

Ilustrasi berbagi data ([Wikimedia Commons, CC-0](https://upload.wikimedia.org/wikipedia/commons/6/65/To_deposit_or_not_to_deposit%2C_that_is_the_question_-_journal.pbio.1001779.g001.png))

# Penutup

Empat hal di atas adalah yang termudah menurut kami untuk dilakukan oleh mahasiswa S1. Kami akan tambahkan bila sekiranya ada teknik berbagi informasi lain yang kami temukan.

Terima kasih telah berkunjung.